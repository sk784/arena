import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:transparent_image/transparent_image.dart';

import 'Place.dart';


class PhotoGrid extends StatelessWidget {
  Place place;


  PhotoGrid(this.place);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GridView.count(crossAxisCount: 3,
          padding: EdgeInsets.only(top: 16.0),
          crossAxisSpacing: 2.0,
          mainAxisSpacing: 2.0,
          children: List.generate(place.customImages.length, (index){
            return InkWell(
              child: Container(
                child: FadeInImage.memoryNetwork(placeholder: kTransparentImage, image: place.customImages[index].fullImage,
                  fit: BoxFit.cover,
                ),
              ),
              onTap: (){
                Navigator.push(context, CupertinoPageRoute(builder: (_) {
                  return DetailScreen(place,place.customImages[index].fullImage);
                }));
              },
            );
          })
      ),
    );
  }
}

class DetailScreen extends StatefulWidget {

  final Place place;
  final String image;
  DetailScreen(this.place,this.image);

  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen>{

 int _photoIndex = 0;
 bool isFirstPhoto = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: MediaQuery
              .of(context)
              .size
              .width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  height: 90,
                  child: DragTarget(builder:
                      (context, List<Place> cd, rd) {
                    return Container();
                  },
                      onWillAccept: (data) {
                        return true;
                      },
                      onAccept: (data) {
                        Navigator.pop(context);
                      }
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Container(
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      height: MediaQuery
                          .of(context)
                          .size
                          .height - 240,
                  child: Stack(
                   children: [
                    Draggable(
                      data:widget.place,
                      feedback: SizedBox(
                        height: 400,
                        width: MediaQuery
                            .of(context)
                            .size
                            .width,
                        child: PageView.builder(
                  pageSnapping: true,
                    itemCount: widget.place.customImages.length,
                    onPageChanged: (int index) {
                        setState(() {
                          _photoIndex = index;
                          isFirstPhoto = false;
                        });
                    },
                    itemBuilder: (context, index) {
                        return PhotoView(imageProvider: FadeInImage
                            .memoryNetwork(placeholder: kTransparentImage,
                            image: isFirstPhoto? widget.image:widget.place.customImages[index].fullImage)
                            .image,
                          backgroundDecoration: BoxDecoration(
                              color: Colors.white.withAlpha(120)),
                          minScale: PhotoViewComputedScale.contained,
                          maxScale: PhotoViewComputedScale.covered * 1.1,);
                    },
                  ),
                      ),
                      childWhenDragging: Container(),
                      child: SizedBox(
                        height: 400,
                        width: MediaQuery
                            .of(context)
                            .size
                            .width,
                        child: PageView.builder(
                          pageSnapping: true,
                          itemCount: widget.place.customImages.length,
                          onPageChanged: (int index) {
                            setState(() {
                              _photoIndex = index;
                              isFirstPhoto = false;
                            });
                          },
                          itemBuilder: (context, index) {
                            return PhotoView(imageProvider: FadeInImage
                                .memoryNetwork(placeholder: kTransparentImage,
                                image: isFirstPhoto? widget.image:widget.place.customImages[index].fullImage)
                                .image,
                              backgroundDecoration: BoxDecoration(
                                  color: Colors.white.withAlpha(120)),
                              minScale: PhotoViewComputedScale.contained,
                              maxScale: PhotoViewComputedScale.covered * 1.1,);
                          },
                        ),
                      ),
                    ),
                    InkWell(
                      child: Container(
                        margin: EdgeInsets.only(left: 16),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(40)
                        ),
                        width: 40,
                        height: 40,
                        child:  Tab(
                            icon: new Image.asset("assets/images/arrowWithBack.png")
                        ),
                      ),
                      onTap: (){
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ),
              )
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  height: 150,
                  child: DragTarget(builder:
                      (context, List<Place> cd, rd) {
                    return Container();
                  },
                      onWillAccept: (data) {
                        return true;
                      },
                      onAccept: (data) {
                        Navigator.pop(context);
                      }
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}